# gitlab-runner helper v17.3.1
FROM registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:x86_64-66269445
RUN apk add rsync openssh curl
RUN curl -sL https://sentry.io/get-cli/ | bash
