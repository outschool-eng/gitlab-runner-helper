# Gitlab Runner Helper

This repo contains a Dockerfile for Gitlab Runner Helper image.

The image we are building simple builds off of the base image and adds RSYNC and OPENSSH packages.

##CHANGELOG
- 02/13/2022: added curl and sentry-cli packages


